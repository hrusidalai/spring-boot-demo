package com.example.zuulproxy;

import java.security.Principal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

	@RequestMapping("/user")
	public Principal user(Principal principal) {
		return principal;
	}

	@GetMapping("/user/me")
	public Principal userData(Principal principal) {
		return principal;
	}

	/*@RequestMapping("/")
	public String login() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		System.out.println(auth.getPrincipal());
		return "index";
	}

	@RequestMapping("/callback")
	public String callback() {
		System.out.println("redirecting to home page");
		return "home";
	}*/
}
