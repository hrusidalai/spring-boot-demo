package com.example.eurekaclient1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/service")
public class MyService {
	@RequestMapping("/names")
	public List<String> getNames() {
		List<String> nameList = new ArrayList<>();
		nameList.add("from CLient1");
		nameList.add("One");
		nameList.add("Two");

		nameList.add("Three");

		return nameList;
	}
}
